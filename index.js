/* Автор - Пряхин Игорь  ::  BART96  ::  Author - Prjakhin Igor */
/* Уважайте чужой труд.  ::  Respect other peoples work. */

'use strict';

if (parseInt(process.version.slice(1)) < 9) throw new Error('Node.js 9.0.0 or higher is required. Update Node.js on your system.');

require('./app/app.js');
