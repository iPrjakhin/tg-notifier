const _Event = require('./_Event.js');

module.exports = class extends _Event {
  constructor() {
    super();
    this.log = (...args) => this.logger.log(...args);
  }

  async execute(err) {
    this.log(`Polling error:`, err);
  }
}
