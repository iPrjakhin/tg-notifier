const request = require('request');

const _Service = require('./_Service.js');


module.exports = class extends _Service {
  constructor(dataBase) {
    super();

    this.dataBase = this._transform(dataBase);
  }

  async execute() {
    for(let author in this.dataBase) {
      for(let repos in this.dataBase[author]) {
        request(this._getOptions(author, repos), (err, res, body) => this._isError(err, body, this._processing));
      }
    }
  }


  _processing(json) {
    console.log(json);
  }


  _getOptions(author, repos) {
    this.options.url = `https://api.github.com/repos/${author}/${repos}/commits?since=${this._getDate(-5115)}&per_page=10`;

    return this.options;
  }


  _transform(data) {
    let newData = {};

    for(let userID in data) {
      let value = JSON.parse(data[userID]);
      for(let author in value) this._createObj([author, value[author], [userID]], newData);
    }

    return newData;
  }

};
