const request = require('request');

const _Service = require('./_Service.js');


module.exports = class extends _Service {
  constructor(dataBase) {
    super();

    this.dataBase = this._transform(dataBase);
  }

  async execute() {
    for(let url in this.dataBase) {
      for(let token in this.dataBase[url]) {
        request(this._getOptions(url, token), (err, res, body) => this._isError(err, body, this._processing));

        // for(let userID in this.dataBase[url][token]) {
        //   let ignore = this.dataBase[url][token][userID];
        // }
      }
    }
  }


  _processing(json) {
    console.log(json);
  }


  _getOptions(url, token) {
    this.options.url = `${url}/rest/issue?max=10&filter=created%3A+${this._getDate(-this.config.interval - 5)}+..+${this._getDate(-5)}`;
    this.options.headers['Authorization'] = 'Bearer '+ token;

    return this.options;
  }


  _transform(data) {
    let newData = {};

    for(let userID in data) {
      let value = JSON.parse(data[userID]);

      for(let url in value) {
        for(let token in value[url]) {
          let ignore = value[url][token].ignore;

          this._createObj([url, token, userID, ignore], newData);
        }
      }
    }

    return newData;
  }

};
