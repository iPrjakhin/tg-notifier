const container = require('bart96-container');


module.exports = class {
  constructor({
    url = null,
    method = 'get',
    headers = {},
  } = {}) {
    this.config = container.get('config');
    this.options = {url, method, headers:Object.assign({'Accept':'application/json', 'User-Agent':'Awesome-Octocat-App'}, headers)};
  }


  _isError(err, body, cb) {
    if (!err) return cb(JSON.parse(body));

    console.log(err.message || err);
    this._send('<b>Ошибка:</b> '+ this._filter(err.message || err));
  }


  _getDate(editMin = 0) {
    let now = new Date(new Date().toLocaleString('ru-RU'));
    let date = new Date(now.setMinutes(now.getMinutes() + editMin));

    let year = date.getFullYear();
    let month = date.getMonth() + 1 > 9 ? date.getMonth() + 1 : '0'+ (date.getMonth() + 1);
    let day = date.getDate() > 9 ? date.getDate() : '0'+ date.getDate();
    let hours = date.getHours() > 9 ? date.getHours() : '0'+ date.getHours();
    let minutes = date.getMinutes() > 9 ? date.getMinutes() : '0'+ date.getMinutes();

    return `${year}-${month}-${day}T${hours}:${minutes}`;
  }


  _filter(str) {
    return str.replace('<', '&lt;').replace('>', '&gt;').replace('&', '&amp;');
  }


  _send(text, {parse_mode='html', reply_markup=null, disable_notification=false} = {}) {
    this.config.users.forEach(user => this.bot.sendMessage(user, text.replace(), {parse_mode, reply_markup, disable_notification})
      .catch(err => this.log(err.message || err, '—', user))
    );
  }


  _createObj(keys, joinObj) {
    let value = keys.pop();

    if (!joinObj) return keys.reverse().reduce((value, key) => new Object({[key]: value}), value);

    return keys.reduce((obj, key, index) => {
      if (index == keys.length - 1) {
        if (Array.isArray(value) && obj[key]) this._unique(obj[key], value[0]);
        else obj[key] = value;

        return joinObj;
      }

      if (!obj[key]) obj[key] = {};
      return obj[key];
    }, joinObj);
  }


  _unique(arr, elem) {
    arr.includes(elem) ? arr : arr.push(elem);
    return arr;
  }

}
