const {Extra, Markup} = require('telegraf');
const _Action = require('./_Action.js');

module.exports = class extends _Action {
  constructor() {
    super();

    this.log = (...args) => this.logger.log(...args);
  }

  async execute(ctx) {
    ctx.editMessageText('Отлично, какой сервис тебя интересует?', Markup.inlineKeyboard([
      Markup.callbackButton('YouTrack', 'youTrack'),
      Markup.callbackButton('GitHab', 'gitHab'),
    ]).extra());
    //.then(() => ctx.answerCbQuery());
  }
}
