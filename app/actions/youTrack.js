const {Extra, Markup} = require('telegraf');
const _Action = require('./_Action.js');

const WizardScene = require('telegraf/scenes/wizard');
const Composer = require('telegraf/composer');
const Stage = require('telegraf/stage')

module.exports = class extends _Action {
  constructor() {
    super();

    this.log = (...args) => this.logger.log(...args);
  }

  async execute(ctx, next) {
    this.config.msgWaiting[ctx.update.callback_query.from.id] = 'ytUrl_ytPerm';

    ctx.editMessageText('Пришли мне <b>адрес</b> и <a href="https://www.jetbrains.com/help/youtrack/incloud/Manage-Permanent-Token.html#obtain-permanent-token">токен</a> в данном формате:\n<code>https://domain.com/dir/tracker\nperm:QVDFk5.ddGzA==.c3pYUR3pYra4</code>', {parse_mode:'HTML'});

    // ctx.reply('YouTrack:', {reply_markup: JSON.stringify({force_reply:true})})//.then(msg => ctx.deleteMessage(msg.message_id));
  }
}
