const container = require('bart96-container');

module.exports = class {
  constructor() {
    this.config = container.get('config');
    this.logger = container.get('logger');
  }
}
