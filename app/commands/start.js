const {Extra, Markup} = require('telegraf');
const {waterfall} = require('async');

const _Command = require('./_Command.js');

module.exports = class extends _Command {
  constructor() {
    super();

    this.log = (...args) => this.logger.log(...args);
  }

  async execute(ctx, next) {
    let user = ctx.update.message.chat;

    // ctx.replyWithHTML(`<b>Привет, ${user.first_name || user.username}!</b>\nУ тебя еще нет активных подписок. Я помогу тебе добавить их. Кликни по кнопке ниже для продолжения обучения =<b>^_^</b>=`,
    //   Markup.keyboard(['/simple', '/inline'], {columns: 2}).resize().extra()
    // )

    ctx.replyWithHTML(
      `<b>Привет, ${user.first_name || user.username}!</b>\nУ тебя еще нет активных подписок. Я помогу тебе добавить их. Кликни по кнопке ниже для продолжения обучения =<b>^_^</b>=`,
      Markup.inlineKeyboard([Markup.callbackButton('Добавить подписку', 'subscribe')]).extra()
    ).catch(this.log);
  }
}
