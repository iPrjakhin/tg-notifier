const _Command = require('./_Command.js');

module.exports = class extends _Command {
  constructor() {
    super();

    this.log = (...args) => this.logger.log(...args);
  }

  async execute(msg, args) {

  }
}
