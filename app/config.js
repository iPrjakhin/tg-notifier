class Config {
  constructor() {
    this.debug = false;
    this.tgToken = 'T O K E N';

    this.redis = {
      host: '127.0.0.1',
      port: '6379',
      password: 'secret',
      db: '2',
      prefix: 'services:tgNotifier:'
    };

    this.interval = 15; // Minutes
    this.msgWaiting = {},



    this.ytToken = 'perm:T O K E N';
    this.baseURL = 'http://tracker.*****.ru/rest/issue/';

    this.users = [
      '185464108',  // BART96
      '67509786',   // oDD1
    ];



    this.priority = [
      {icon:'🔥', notification:true, name:'Неотложная'},
      {icon:'💥', notification:true, name:'Критическая'},
      {icon:'⚡️' , notification:false, name:'Серьезная'},
      {icon:'☘️', notification:false, name:'Обычная'},
      {icon:'💧', notification:false, name:'Незначительная'},
    ];

    this.ignoreProjects = [
      'arhive',
    ];

  }
}



module.exports = Config;
