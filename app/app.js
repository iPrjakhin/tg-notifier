/* Автор - Пряхин Игорь  ::  BART96  ::  Author - Prjakhin Igor */
/* Уважайте чужой труд.  ::  Respect other peoples work. */

'use strict';

const Telegraf = require('telegraf');
const {Extra, Markup} = require('telegraf');

const Logger = require('bart96-style-cli-ultimate');
const container = require('bart96-container');
const RedisLib = require('redis');
const {waterfall} = require('async');
const {readdir} = require('fs');

const Config = require('./config.js');


class App {
  constructor() {
    container.set('config', new Config());
    this.config = container.get('config');

    container.set('logger', new Logger({debugMode:this.config.debug}));
    this.log = (...args) => container.get('logger').log(...args);
    this.log('{white Starting tgNotifier v2}', 3);

    this.client = new Telegraf(this.config.tgToken);

    // this.client.on('text', ctx => console.log(ctx.update));

    // this.client.command('random', (ctx) => {
    //   return ctx.reply('random example',
    //     Markup.inlineKeyboard([
    //       Markup.callbackButton('Coke', 'Coke'),
    //       Markup.callbackButton('Dr Pepper', 'Dr Pepper', Math.random() > 0.5),
    //       Markup.callbackButton('Pepsi', 'Pepsi')
    //     ]).extra()
    //   )
    // }).action('Dr Pepper', (ctx, next) => {
    //   return ctx.reply('👍').then(() => next())
    // })

    // const markup = Extra
    //   .HTML()
    //   .markup((m) => m.inlineKeyboard([
    //     m.callbackButton('Add 1', 'add:1'),
    //     m.callbackButton('Add 10', 'add:10'),
    //     m.callbackButton('A*dd* 100', 'add:100'),
    //     m.callbackButton('Subtract 1', 'sub:1'),
    //     m.callbackButton('Subtract 10', 'sub:10'),
    //     m.callbackButton('Subtract 100', 'sub:100'),
    //     m.callbackButton('🐈', Math.random().toString(36).slice(2)),
    //     m.callbackButton('Clear', 'clear')
    //   ], { columns: 3 }))
    //
    // this.client.start((ctx) => ctx.reply('Welcome!', markup))
    // this.client.help((ctx) => ctx.reply('Send me a sticker', {
    //   parse_mode: 'HTML',
    //   reply_markup: JSON.stringify({
    //       one_time_keyboard: false,
    //       resize_keyboard: true,
    //       keyboard: [
    //           ['Добавить подписку', 'Список подписок', 'Удалить подписку'],
    //           ['Помощь']
    //       ]
    //   })}));
    // this.client.on('sticker', (ctx) => ctx.reply('👍'))
    // this.client.hears('hi', (ctx) => ctx.reply('Hey there'))
    // this.client.hears(/buy/i, (ctx) => ctx.reply('Buy-buy'))
    // this.client.command('foo', (ctx) => console.log(ctx))



    // this.client.use(Telegraf.log())

// this.client.command('onetime', ({ reply }) =>
//   reply('One time keyboard', Markup
//     .keyboard(['/simple', '/inline', '/pyramid'])
//     .oneTime()
//     .resize()
//     .extra()
//   )
// )
//
// this.client.command('custom', ({ reply }) => {
//   return reply('Custom buttons keyboard', Markup
//     .keyboard([
//       ['🔍 Search', '😎 Popular'], // Row1 with 2 buttons
//       ['☸ Setting', '📞 Feedback'], // Row2 with 2 buttons
//       ['📢 Ads', '⭐️ Rate us', '👥 Share'] // Row3 with 3 buttons
//     ])
//     .oneTime()
//     .resize()
//     .extra()
//   )
// })
//
// this.client.hears('🔍 Search', ctx => ctx.reply('Yay!'))
// this.client.hears('📢 Ads', ctx => ctx.reply('Free hugs. Call now!'))
//
// this.client.command('special', (ctx) => {
//   return ctx.reply('Special buttons keyboard', Extra.markup((markup) => {
//     return markup.resize()
//       .keyboard([
//         markup.contactRequestButton('Send contact'),
//         markup.locationRequestButton('Send location')
//       ])
//   }))
// })
//
// this.client.command('pyramid', (ctx) => {
//   return ctx.reply('Keyboard wrap', Extra.markup(
//     Markup.keyboard(['one', 'two', 'three', 'four', 'five', 'six'], {
//       wrap: (btn, index, currentRow) => {
//         console.log(btn, index, currentRow);
//         currentRow.length >= (index + 1) / 2
//       }
//     })
//   ))
// })
//
// this.client.command('simple', (ctx) => {
//   return ctx.replyWithHTML('<b>Coke</b> or <i>Pepsi?</i>', Extra.markup(
//     Markup.keyboard(['Coke', 'Pepsi'])
//   ))
// })
//
// this.client.command('inline', (ctx) => {
//   return ctx.reply('<b>Coke</b> or <i>Pepsi?</i>', Extra.HTML().markup((m) =>
//     m.inlineKeyboard([
//       m.callbackButton('Coke', 'Coke'),
//       m.callbackButton('Pepsi', 'Pepsi')
//     ])))
// })

// this.client.command('random', (ctx) => {
//   return ctx.reply('random example',
//     Markup.inlineKeyboard([
//       Markup.callbackButton('Coke', 'Coke'),
//       Markup.callbackButton('Dr Pepper', 'Dr Pepper'),
//       Markup.callbackButton('Pepsi', 'Pepsi')
//     ]).extra()
//   )
// })

// this.client.command('caption', (ctx) => {
//   return ctx.replyWithPhoto({ url: 'https://picsum.photos/200/300/?random' },
//     Extra.load({ caption: 'Caption' })
//       .markdown()
//       .markup((m) =>
//         m.inlineKeyboard([
//           m.callbackButton('Plain', 'plain'),
//           m.callbackButton('Italic', 'italic')
//         ])
//       )
//   )
// })

// this.client.hears(/\/wrap (\d+)/, (ctx) => {
//   return ctx.reply('Keyboard wrap', Extra.markup(
//     Markup.keyboard(['one', 'two', 'three', 'four', 'five', 'six'], {
//       columns: parseInt(ctx.match[1])
//     })
//   ))
// })

// this.client.action('Dr Pepper', (ctx, next) => {
//   return ctx.reply('👍').then(() => next())
// })

// this.client.action('plain', async (ctx) => {
//   ctx.editMessageCaption('Caption', Markup.inlineKeyboard([
//     Markup.callbackButton('Plain', 'plain'),
//     Markup.callbackButton('Italic', 'italic')
//   ]))
// })
//
// this.client.action('italic', (ctx) => {
//   ctx.editMessageCaption('_Caption_', Extra.markdown().markup(Markup.inlineKeyboard([
//     Markup.callbackButton('Plain', 'plain'),
//     Markup.callbackButton('* Italic *', 'italic')
//   ])))
// })
//
// this.client.action(/.+/, (ctx) => {
//   return ctx.answerCbQuery(`Oh, ${ctx.match[0]}! Great choice`)
// })




    // this.client.action('subscribe', ctx => ctx.reply(
    //   'Отлично, какой сервис тебя интересует?',
    //   Markup.inlineKeyboard([
    //     Markup.callbackButton('YouTrack', 'youTrack'),
    //     Markup.callbackButton('GitHab', 'gitHab'),
    //   ]).extra()
    // ));

    this.start();
  }


  start() {
    waterfall([

      // >>> Redis <<<
      next => {
        container.set('redis', RedisLib.createClient(this.config.redis));
        this.redis = container.get('redis')
          .on('error',   err => this.log(`{red Error} \u27A4 ${err}`, -7))
          .on('warning', err => this.log(`{yellow Warn} \u27A4 ${err}`, 7))
          .on('ready',   () => {this.log('Ready', 7); next(null)});
      },

      // >>> DataBase <<<
      next => this.redis.keys('*', (err, reply) => next(err, reply.map(key => key.split(':').pop()))),
      (keys, next) => {
        let multi = this.redis.multi();
        keys.forEach(key => multi.hgetall(key));
        multi.exec((err, reply) => next(err, keys, reply));
      },

      (keys, values, next) => {
        container.set('dataBase', keys.reduce((obj, key, index) => {
          obj[key] = values[index];
          return obj;
        }, Object.create(null)));

        next(null);
      },


      // >>> Action <<<
      next => readdir('./app/actions', (err, files) => next(err, files.filter(file => !file.startsWith('_')))),
       (files, next) => {
         this.log(`Loading a total of ${files.length} actions.`, 3);

         files.forEach(file => {
           try {
             let actionName = file.split('.')[0];
             let action = new (require(`./actions/${file}`))();

             this.client.action(actionName, (...args) => action.execute(...args));
             this.log(`✔ ${file}`, 3);
           } catch(err) {this.log(`✖ ${file}`, err)}
         });

         next(null);
       },


      // >>> Commands <<<
     next => readdir('./app/commands', (err, files) => next(err, files.filter(file => !file.startsWith('_')))),
      (files, next) => {
        this.log(`Loading a total of ${files.length} commands.`, 3);

        files.forEach(file => {
          try {
            let commandName = file.split('.')[0];
            let command = new (require(`./commands/${file}`))();

            this.client.command(commandName, (...args) => command.execute(...args));
            this.log(`✔ ${file}`, 3);
          } catch(err) {this.log(`✖ ${file}`, err)}
        });

        next(null);
      },

      // >>> Events <<<
      next => readdir('./app/events', (err, files) => next(err, files.filter(file => !file.startsWith('_')))),
      (files, next) => {
        this.log(`Loading a total of ${files.length} events.`, 3);

        files.forEach(file => {
          try {
            let eventName = file.split('.')[0];
            let event = new (require(`./events/${file}`))();

            this.client.on(eventName, (...args) => event.execute(...args));
            this.log(`✔ ${file}`, 3);
          } catch(err) {this.log(`✖ ${file}`, err)}
        });

        next(null);
      },

      // >>> Services <<<
      next => readdir('./app/services', (err, files) => next(err, files.filter(file => !file.startsWith('_')))),
      (files, next) => {
        this.log(`Loading a total of ${files.length} services.`, 3);

        let services = [];
        let dataBase = container.get('dataBase');

        files.forEach(file => {
          try {
            let serviceName = file.split('.')[0];
            services.push(new (require(`./services/${file}`))(dataBase[serviceName]));
            this.log(`✔ ${file}`, 3);
          } catch(err) {this.log(`✖ ${file}`, err)}
        });

        setTimeout(services => services.forEach(service => service.execute()), 1000, services);

        next(null);
      },

    ],

    (err, result) => {
      if (err) return this.log(err, -1);

      this.testing();
      this.finish();
    });
  }


  testing() {
    this.log('{white Testing components...}', 3);
  }


  finish() {
    this.client.startPolling();
    this.log('{white Сompleted successfully\n}', 3);
  }

}



let app = new App();
module.exports = app;
